"""
Global (default) settings for the test-bed project.
Edit here if you want to change the default behaviour.
"""
import os

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_SET = os.path.join(PROJECT_DIR, "payloads.txt")
MAX_PAYLOAD_SIZE = 1024 # in bytes for all protocols
SERVER_ADDRESS = "192.168.178.22"
WEBSOCKET_SERVER_PORT = 7070
COAP_SERVER_PORT = 8080
COAP_SERVER_PATH = "store/"
MQTT_SERVER_PORT = 9090
MQTT_PUBLISH_TOPIC = "store"

def utf8len(string):
    """Return the size of the given string in bytes"""
    return len(string.encode('utf-8'))
