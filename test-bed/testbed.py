"""Testbed project entrypoint."""
import settings
import argparse

def main():
    """Launch the testbed"""
    main_parser = argparse.ArgumentParser(prog=__file__)
    subparsers = main_parser.add_subparsers(
        title="actions", dest="action"
    )
    client_parser = subparsers.add_parser("client")
    server_parser = subparsers.add_parser("server")

    available_clients = [
        "websocket_single",
        "websocket_multiple",
        "coap_nonconfirmable",
        "coap_confirmable",
        "mqtt_qos_0",
        "mqtt_qos_1",
        "mqtt_qos_2"
    ]
    client_parser.add_argument("--category",
                               choices=available_clients,
                               dest="category",
                               required=True)
    available_servers = [
        "websocket",
        "coap_piggybacked_ack",
        "coap_piggybacked_confirmable",
        "coap_piggybacked_nonconfirmable",
        "coap_separated_confirmable",
        "coap_separated_nonconfirmable",
        "mqtt"
    ]
    server_parser.add_argument("--category",
                               choices=available_servers,
                               dest="category",
                               required=True)

    args = main_parser.parse_args()
    if args.action == "client":
        execute_client(args.category)
    elif args.action == "server":
        execute_server(args.category)

def execute_client(category):
    import clients
    if category == "websocket_single":
        clients.websocket_single_connection.start()
    elif category == "websocket_multiple":
        clients.websocket_multiple_connections.start()
    elif category == "coap_confirmable":
        clients.coap.start(confirmable=True)
    elif category == "coap_nonconfirmable":
        clients.coap.start(confirmable=False)
    elif category == "mqtt_qos_0":
        clients.mqtt.start(qos_level=0)
    elif category == "mqtt_qos_1":
        clients.mqtt.start(qos_level=1)
    elif category == "mqtt_qos_2":
        clients.mqtt.start(qos_level=2)

def execute_server(category):
    import servers
    if category == "websocket":
        servers.websocket.start()
    elif category == "coap_piggybacked_ack":
        servers.coap.start(piggybacked=True, confirmable=None)
    elif category == "coap_piggybacked_confirmable":
        servers.coap.start(piggybacked=True, confirmable=True)
    elif category == "coap_piggybacked_nonconfirmable":
        servers.coap.start(piggybacked=True, confirmable=False)
    elif category == "coap_separated_confirmable":
        servers.coap.start(piggybacked=False, confirmable=True)
    elif category == "coap_separated_nonconfirmable":
        servers.coap.start(piggybacked=False, confirmable=False)
    elif category == "mqtt":
        servers.mqtt.start()

if __name__ == "__main__":
    main()
