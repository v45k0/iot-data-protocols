import os
import sys
sys.path.insert(0, os.path.dirname(__file__))

import settings
from websocket import create_connection

def start():
    address = "ws://%s:%d" % (
        settings.SERVER_ADDRESS,
        settings.WEBSOCKET_SERVER_PORT,
    )
    print("Connected to the server!")
    print("Sending the dataset one payload in separate connections")
    with open(settings.DATA_SET, "r") as data_set:
        for payload in data_set:
            connection = create_connection(address)
            connection.send(payload)
            connection.close()
    print("Done.")
