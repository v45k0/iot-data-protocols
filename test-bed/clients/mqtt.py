import os
import sys
sys.path.insert(0, os.path.dirname(__file__))
import time
import json
import settings
import paho.mqtt.client as mqtt

def on_connect(client, userdata, flags, result_code):
    print("Connected to broker with result code %s" % str(result_code))

def start(qos_level=0):
    print("MQTT client with QoS%d started" % qos_level)
    client = mqtt.Client(client_id="client-1")
    client.will_set(
        "status",
        payload=json.dumps({"status": "offline"}),
        retain=True
    )
    client.on_connect = on_connect
    client.connect(settings.SERVER_ADDRESS, settings.MQTT_SERVER_PORT)
    client.loop(timeout=60.0)
    try:
        print("Publishing each generated payload")
        data_set = open(settings.DATA_SET, "r")
        counter = 1
        for payload in data_set:
            print("Sending packet #%d (%d Bytes)..." % (counter, settings.utf8len(payload)))
            client.publish(
                topic=settings.MQTT_PUBLISH_TOPIC,
                payload=payload,
                qos=qos_level
            )
            client.loop(timeout=60.0)
            time.sleep(1)
            counter += 1
    finally:
        data_set.close()
        client.disconnect()
        print("Done.")
