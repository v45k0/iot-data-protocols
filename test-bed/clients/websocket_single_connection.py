import os
import sys
sys.path.insert(0, os.path.dirname(__file__))
import time
import settings
from websocket import create_connection

def start():
    address = "ws://%s:%d" % (
        settings.SERVER_ADDRESS,
        settings.WEBSOCKET_SERVER_PORT,
    )
    connection = create_connection(address)
    print("Connected to the server!")
    print("Sending the dataset one payload at a time in a single run")
    with open(settings.DATA_SET, "r") as data_set:
        counter = 1
        for payload in data_set:
            print("Sending packet #%d (%d Bytes)..." % (counter, settings.utf8len(payload)))
            connection.send(payload)
            time.sleep(1)
            counter += 1
    connection.close()
    print("Done.")
