\documentclass[NET,english,beameralt]{tumbeamer}
% If you load additional packages, do so in packages.sty as figures are build
% as standalone documents and you may want to have effect on them, too.

% Folder structure:
% .
% ├── beamermods.sty                  % depricated an will be removed soon
% ├── compile                         % remotely compile slides
% ├── figures                         % all figures go here
% │   └── schichtenmodelle_osi.tikz   % each .tikz or .tex is a target
% ├── include                         % create your document here
% │   ├── example.tex                 % example document
% │   └── slides.tex                  % make document wide changes here
% ├── lit.bib                         % literature
% ├── Makefile
% ├── moeptikz.sty                    % fancy networking symbols
% ├── packages.sty                    % load additional packages there
% ├── pics                            % binary pictures go here
% ├── slides.tex                      % main document (may be more than one)
% ├── tumbeamer.cls
% ├── tumcolor.sty                    % TUM color definitions
% ├── tumcontact.sty                  % TUM headers and footers
% ├── tumlang.sty                     % TUM names and language settings
% └── tumlogo.sty                     % TUM logos


% Configure author, title, etc. here:
\input{include/slides}
\pgfplotsset{
    legend image with text/.style={
        legend image code/.code={%
            \node[anchor=center] at (0.3cm,0cm) {#1};
        }
    },
}
\begin{document}

% If you are preparing a talk but do not like the default font sizes, you may
% want to try the class option 'beameralt', which uses smaller default font
% sizes and integrates subsection/subsubsection names into the headline.

% For lecture mode, you may want to build one set of slides per chapter but
% with common page numbering. If so,
% 1) create a new .tex file for each chapter, e.g. slides_chapN.tex,
% 2) set the part counter to N-1 (assuming chapters start at 0), and
% 3) and name your chapter by using the \part{} command.
%\setcounter{part}{-1}
%\part{Organisatorisches und Einleitung}

% For 16:9 slides, use the class option 'aspectratio=169'.

% If class option 'noframenumbers' is given, frame numbers are not printed.

% If class option 'notitleframe' is given, the title frame is not autmatically
% generated.

% Class option 'nocontentframes' suppresses automatic generation of content
% frames when new parts/sections are started.

% Include source files from ./include (or ./include/chapN).
\input{include/agenda}


%\input{include/introduction}

\section{Introduction}

\subsection{The Internet of Things}
\begin{frame}
\begin{itemize}
    \item Now: The Internet as a resource for humans
    \item Next: The \emph{Internet} as a resource for \emph{things} (machines)
\end{itemize}
\begin{exampleblock}{}
  {\large ``The Internet of Things (or IoT) is what we get when we connect Things, that are not operated by humans, to the Internet.''}
  \vskip5mm
  \hspace*\fill{\small--- XMPP Foundation \footnote{\url{https://xmpp.org/uses/internet-of-things.html}}}
\end{exampleblock}
\begin{figure}
    \centering
    \includegraphics[scale=0.55]{pics/nodes}
\end{figure}
\end{frame}

\subsection{Constrained Communication}
\begin{frame}
\begin{itemize}
    \item IoT devices are constrained: lack of CPU power, RAM, energy
    \item Networks are constrained: unreliability, low bandwidth (e.g. NBIoT, Sigfox, BLE)
    \item $\Rightarrow$ Optimal communication mechanism (protocols, algorithms) is desired
\end{itemize}
\begin{minipage}{0.35\linewidth}
\begin{figure}
    \def\svgwidth{\linewidth}
	\import{figures/}{ic_package_normal.pdf_tex}
	\caption{An example of a normal sized SoC}
	\label{fig:normal_ic}
\end{figure}
\end{minipage}
\footnote{
IC image: \url{https://commons.wikimedia.org/wiki/File:Ic-package-CDIP.svg}
}
\hfill
\begin{minipage}{0.20\linewidth}
\begin{figure}
    \def\svgwidth{\linewidth}
	\import{figures/}{ic_package_tiny.pdf_tex}
	\caption{An example for a very tiny SoC}
	\label{fig:tiny_ic}
\end{figure}
\end{minipage}
\footnote{
Tiny IC image: \url{https://commons.wikimedia.org/wiki/File:Ic-package-HEXDIP.svg}
}
\end{frame}

\subsection*{Evaluation Model}
\begin{frame}
For each protocol, we assume/calculate:
\begin{itemize}
    \item No latency, only throughput ($\tau$)
    \item No encryption, cache or proxy optimizations
    \item No IP fragmentation
    \item Pure protocol overhead approximation (L4 upwards): \\
    $\omega(n)$ for $n$ communication slots
    \item Only upstream communication (e.g. storing sensor data)
\end{itemize}
\vfill
\pause
Protocol overhead model function:
\begin{equation}
\label{eq:overhead}
\begin{aligned}
\omega(n) = bH_o + bH_c + nh 
\end{aligned}
\end{equation}
Throughput model function:
\begin{equation}
\label{eq:throughput}
\begin{aligned}
\tau(n,\widetilde{x})= \dfrac{n\widetilde{x}}{\omega(n)+n\widetilde{x}+bp(c_1 + c_2)+np}
\end{aligned}
\end{equation}
\end{frame}

%\input{include/protocols}

\section{Application Protocols}

\subsection{WebSocket}
\begin{frame}
\begin{itemize}
    \item RFC 6455 (December 2011) - The WebSocket Protocol
    \item Higher abstraction of TCP for Web applications
    \item $\Rightarrow$ asynchronous communication between client and server (HTTP polling problem)
    \item Fitting in existing HTTP infrastructure
    \item Suitable for real time bidirectional communication in IoT services
\end{itemize}
\begin{minipage}{0.35\linewidth}
\begin{figure}
    \def\svgwidth{\linewidth}
	\import{figures/}{websocket_layering.pdf_tex}
	\caption{Layering of the WebSocket protocol}
	\label{fig:experiment_environment}
\end{figure}
\end{minipage}
\hfill
\begin{minipage}{0.5\linewidth}
\begin{figure}
    \def\svgwidth{\linewidth}
	\import{figures/}{device_server_websocket.pdf_tex}
	\caption{WebSocket based client-server communication}
	\label{fig:websocket_communication}
\end{figure}
\end{minipage}
\begin{itemize}
    \item $\omega_{WebSocket}(n) \approx 600 + 54n$
\end{itemize}
\end{frame}

\subsection{CoAP}
\begin{frame}
\begin{itemize}
    \item RFC 7257 (June 2014) - Constrained Application Protocol
    \item Intended for machine-to-machine (M2M) communication in constrained networks
    \item Brings HTTP's RESTful experience to IoT (request-response communication pattern)
    \item Extendable to support publish-subscribe and blockwise transport
    \item Thin messaging layer over the request-response life cycle
\end{itemize}
\begin{minipage}{0.35\linewidth}
\begin{figure}
    \def\svgwidth{\linewidth}
	\import{figures/}{coap_layering.pdf_tex}
	\caption{Layering of the Constrained Application Protocol}
	\label{fig:experiment_environment}
\end{figure}
\end{minipage}
\hfill
\begin{minipage}{0.5\linewidth}
\begin{figure}
    \def\svgwidth{\linewidth}
	\import{figures/}{device_server_coap.pdf_tex}
	\caption{CoAP based RESTful client-server communication}
	\label{fig:websocket_communication}
\end{figure}
\end{minipage}
\begin{itemize}
    \item $38n \le \omega_{CoAP}(n) \le 62n$
\end{itemize}
\end{frame}

\subsection{MQTT}
\begin{frame}
\begin{itemize}
    \item Created in 1999 for satellite connection of oil pipelines
    \item As of 2014 an OASIS and as of 2016 an ISO standard
    \item Publish-subscribe communication pattern in constrained environments
    \item Lightweight and easy to implement on the client side
    \item Three different Quality of Service (QoS) levels
\end{itemize}
\begin{minipage}{0.35\linewidth}
\begin{figure}
    \def\svgwidth{\linewidth}
	\import{figures/}{mqtt_layering.pdf_tex}
	\caption{Layering of the MQTT protocol}
	\label{fig:experiment_environment}
\end{figure}
\end{minipage}
\hfill
\begin{minipage}{0.50\linewidth}
\begin{figure}
    \def\svgwidth{\linewidth}
	\import{figures/}{device_server_mqtt_publish_subscribe.pdf_tex}
	\caption{MQTT based publish-subscribe communication}
	\label{fig:websocket_communication}
\end{figure}
\end{minipage}
\begin{itemize}
    \item $300 + 42n \leq \omega_{MQTT}(n) \leq 300 + 174n$
\end{itemize}
\end{frame}

\section*{Quick Overview}
\begin{frame}
\begin{itemize}
    \item $\omega_{WebSocket}(n) \approx 600 + 54n$                                                                           
    \item $38n \le \omega_{CoAP}(n) \le 62n$
    \item $300 + 42n \leq \omega_{MQTT}(n) \leq 300 + 174n$
\end{itemize}
$\Rightarrow$ CoAP $<$ MQTT $<$ WebSocket?
\vfill
\end{frame}

\input{include/experiment}


%\input{include/summary}
\section{Summary}
\begin{frame}
\begin{itemize}
    \item Communication performance is \emph{very important} for IoT
    \item CoAP $<$ MQTT QoS 0 $<$ WebSocket $<<$ MQTT QoS 2 for normal upstream communication
    \item More application data $\Rightarrow$ higher throughput and better cost payoff
    \item However, protocols with different feature set: \\ 
    REST vs Publish-Subscribe vs Asynchronous Communication
\end{itemize}
\end{frame}

{
  \makeatletter % to change template
    \setbeamertemplate{headline}[default] % not mandatory, but I though it was better to set it blank
    \def\beamer@entrycode{\vspace*{-\headheight}} % here is the part we are interested in :)
\makeatother
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}Thank you for your attention!\par%
  \end{beamercolorbox}
    \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    Email: sarafov@cs.tum.edu \\ \vspace*{0.5cm}
    Source code, paper and talk can be found at: \\ \url{http://home.in.tum.de/~sarafov} \par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}
  
% Include markdown source from ./pandoc
%\input{pandoc/example}

% Comment out if you do not want a bibliography
\section{References}
\begin{frame}[allowframebreaks]
    \nocite{*}\bibliographystyle{abbrv}
    \setbeamertemplate{bibliography item}[text]
    \footnotesize
    \bibliography{lit}
\end{frame}

\input{include/backup}

\end{document}

