def utf8len(string):
    return len(string.encode("utf-8"))

def calculate_websocket_overhead():
    print("======= Calculating WebSocket Overhead ========")

    # coh = client opening handshake header, no body
    coh = "GET /endpoint HTTP/1.1\r\n"
    coh += "Host: server.example.com\r\n"
    coh += "Upgrade: websocket\r\n"
    coh += "Connection: Upgrade\r\n"
    coh += "Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==\r\n"
    coh += "Sec-WebSocket-Version: 13\r\n\n"
    coh_overhead = utf8len(coh)
    print("client opening handshake overhead %d B" % coh_overhead)

    # soh = server opening handshake header, no body
    soh = "HTTP/1.1 101 Switching Protocol\r\n"
    soh += "Upgrade: websocket\r\n"
    soh += "Connection: Upgrade\r\n"
    soh += "Sec-WebSocket-Accept: s3pPLMBiTxaQ9kYGzzhZRbK+xOo=\r\n\n"
    soh_overhead = utf8len(soh)
    print("server opening handshake overhead %d B" % soh_overhead) 

    # the overhead that is needed to frame
    # a single websocket data package (actual payload)
    frame_overhead = 14
    print("message framing overhead %d B" % frame_overhead)

    # tearing down is simply a control frame
    # in our case without any body (simplification)
    # each one sends a closing frame
    # (request closure, acknowledge it)
    cch_overhead = frame_overhead
    print("client closing handshake overhead %d B" % cch_overhead)
    sch_overhead = frame_overhead
    print("server closing handshake overhead %d B" % sch_overhead)

    # each websocket message is packaged in a TCP segment
    print("TCP header overhead 20 B")

    print("===============================================")

def calculate_coap_overhead():
    print("======= Calculating CoAP Overhead ========")
    # we agree on a request/response tokens of length 4
    token_length = 4

    # for disabling the cache, we set the Max-Age option to 0
    # only relevant for messages that carry request/response
    # => option delta = 14, option length = 1, option value = 0
    options_length = 2

    # 1 byte is reserved for 0xff, that signals the start of the payload
    header_overhead = 4 + token_length + options_length + 1
    print("response/request header overhead %d B" % header_overhead)

    # an acknowledgement is basically an empty message
    # (only the plain header) with message ID and ACK flag = 1
    print("ACK message size 4 B")

    # each CoAP message is packaged in a UDP datagram
    print("UDP header overhead 8 B")

    print("==========================================")
    

def calculate_mqtt_overhead():
    print("======= Calculating MQTT Overhead ========")

    # coh = client opening handshake header with will message
    coh_overhead = 50
    print("client opening handshake overhead %d B" % coh_overhead)

    # soh = server opening handshake header, no body
    soh_overhead = 4
    print("server opening handshake overhead %d B" % soh_overhead) 

    # the overhead that is needed to frame
    # a single websocket data package (actual payload)
    frame_overhead = 2
    print("message framing overhead %d B" % frame_overhead)

    # tearing down is simply a control frame
    # in our case without any body (simplification)
    # each one sends a closing frame
    # (request closure, acknowledge it)
    cch_overhead = 2
    print("client closing handshake overhead %d B" % cch_overhead)
    sch_overhead = 0
    print("server closing handshake overhead %d B" % sch_overhead)

    # each websocket message is packaged in a TCP segment
    print("TCP header overhead 20 B")

    print("===============================================")


def main():
    calculate_websocket_overhead()
    print("\n")
    calculate_coap_overhead()
    print("\n")
    calculate_mqtt_overhead()

if __name__ == "__main__":
    main()
